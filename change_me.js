
/**
 * 
 * Diberikan sebuah function changeMe(arr) yang menerima satu parameter
 * berupa array multidimensi dimana array tersebut berisi value (pasti berurut dari kiri ke kanan)
 * First Name, Last Name, Gender dan Birth Year.
 * 
 * Fungsi ini akan menampilkan object literal yang memiliki property firstName, lastName, gender dan age.
 * Nilai age didapatkan dari tahun sekarang dikurang tahun lahir.
 * Jika tahun lahir tidak diisi atau tahun lahir lebih besar dibandingkan tahun sekarang maka age akan berisi 'Invalid Birth Year'
 * 
 * Contoh jika arr inputan adalah [['Platinum', 'Fox', 'female', 1995], ['John', 'Doe', 'male', 2000]] maka output:
 * 
 * Platinum Fox: { firstName: 'Platinum', lastName: 'Fox', gender: 'female', age: 23 }
 * John Doe: { firstName: 'John', lastName: 'Doe', gender: 'male', age: 18 }
 * 
 */


function changeMe(arr) {
    // you can only write your code here!
    const date = new Date();
    const thisYear = date.getFullYear();
    const getAge = (year) => {
        const _year = Number(year);
        if (!_year || isNaN(_year) || _year > thisYear) {
            return "Invalid Birth Year"
        }
        return thisYear - _year;
    }
    arr.forEach(a => {
        const obj = {};
        obj["firstName"] = a[0];
        obj["lastName"] = a[1];
        obj["gender"] = a[2];
        obj["age"] = getAge(a[3]);
        console.log(`${obj.firstName} ${obj.lastName}:`, obj);
    });
}

// TEST CASES
changeMe([['Christ', 'Evans', 'Male', 1982], ['Robert', 'Downey', 'Male']]);
// 1. Christ Evans:
// { firstName: 'Christ',
//   lastName: 'Evans',
//   gender: 'Male',
//   age: 37 }
// 2. Robert Downey:
// { firstName: 'Robert',
//   lastName: 'Downey',
//   gender: 'Male',
//   age: 'Invalid Birth Year' }

changeMe([]); // ""
